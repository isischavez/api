//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req, res, next){
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

  var requestjson = require('request-json');
  var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/bdbanca4m840718/collections/movimientos?apiKey=OyHVw7izv6mqlruHdcv-gJ0t6_I6vqHz";
  var clienteMLab = requestjson.createClient(urlmovimientosMLab);


var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
app.get("/",function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get("/clientes/:idcliente",function(req, res){
  res.send('Aqui tiene al cliente numero : '+ req.params.idcliente);
});

app.post("/", function(req, res){
  res.send('Hemos recibido su peticion de post cambiada');
});
app.put("/", function(req, res){
  res.send('Hemos recibido su peticion de put');
});
app.delete("/", function(req, res){
  res.send('Hemos recibido su peticion de delete');
});
app.get("/movimientos",function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
});
});
app.post("/movimientos", function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body ){
    res.send(body);
  });
});
